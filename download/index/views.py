from django.shortcuts import redirect, render

from django.conf import settings

from django.http import HttpResponse
from .models import File
# Create your views here.
def index(request, file_name):
    print('hiya')
    target_file = File.objects.get(file_name=file_name)
    return redirect(target_file.file_url)