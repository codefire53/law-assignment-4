from django.shortcuts import render
from django.conf import settings
from .models import File
from .catbox import CatboxUploader
import uuid
def index(request):
    if request.method == 'POST':
        myfile = request.FILES['myfile']
        myfile_binary = request.FILES['myfile'].read()
        new_name = str(uuid.uuid1())+'-'+myfile.name

        uploader = CatboxUploader(filename=new_name, file=myfile_binary)
        result = uploader.execute()
        print(result)
        new_file = File(file_name=new_name, file_url=result)
        new_file.save()
        return render(request, 'index.html', {
            'uploaded_file_url': 'http://localhost/download/'+new_name
        })
    return render(request, 'index.html')