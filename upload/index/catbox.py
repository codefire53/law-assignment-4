#Credit: https://github.com/yukinotenshi/pyupload
from .base import Uploader

class CatboxUploader(Uploader):
    def __init__(self, filename, file):
        self.filename = filename
        self.file_host_url = "https://catbox.moe/user/api.php"
        self.file = file

    def execute(self):
        data = {
            'reqtype': 'fileupload',
            'userhash': '',
            'fileToUpload': (self.filename, self.file, self._mimetype())
        }
        response = self._multipart_post(data)

        return response.text