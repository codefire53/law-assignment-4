from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
# Create your models here.
class File(models.Model):
    file_name = models.CharField(max_length=255, unique=True)
    file_url = models.CharField(max_length=255)

